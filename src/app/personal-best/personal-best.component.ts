import { 
  Component,
  OnInit,
  Input
} from '@angular/core';
import { Best } from '../user-service/best';

@Component({
  selector: 'rt-personal-best',
  templateUrl: './personal-best.component.html',
  styleUrls: ['./personal-best.component.css']
})
export class PersonalBestComponent implements OnInit {

  @Input()
  private best: Best;

  constructor() {
  }

  ngOnInit() {
  }

}
