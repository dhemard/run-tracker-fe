import { Component, OnInit } from '@angular/core';
import { UserService } from '../user-service/user.service';
import { HttpErrorResponse } from '@angular/common/http';
import { User } from '../user-service/user';

@Component({
  selector: 'rt-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  private userService: UserService;
  private error: HttpErrorResponse;
  private users: User[];

  constructor(userService: UserService) {
    this.userService = userService;
    userService.getAllUsers().subscribe(
      users => this.users = users,
      error => this.error = error
    );
  }

  ngOnInit() {
  }
}
