import { Component, OnInit } from '@angular/core';
import { RunService } from '../run-service/run.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Run } from '../run-service/run';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'rt-run-list',
  templateUrl: './run-list.component.html',
  styleUrls: ['./run-list.component.css']
})
export class RunListComponent implements OnInit {
  private error: HttpErrorResponse;
  private userId: number;
  private runs: Run[];

  constructor(runService: RunService, route: ActivatedRoute) {
    route.params.subscribe(
      params => {
        this.userId = params['userId'];
        runService.getAllRuns(this.userId).subscribe(
          runs => this.runs = runs,
          error => this.error = error
        );
      }
    )
  }

  ngOnInit() {
  }

}
