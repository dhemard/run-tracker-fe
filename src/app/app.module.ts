import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from "@angular/router";
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from "@angular/forms";


import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HeaderLinksComponent } from './header-links/header-links.component';
import { UserService } from './user-service/user.service';
import { UserListComponent } from './user-list/user-list.component';
import { UserInfoComponent } from './user-info/user-info.component';
import { PersonalBestComponent } from './personal-best/personal-best.component';
import { UserFormComponent } from './user-form/user-form.component';
import { RunService } from './run-service/run.service';
import { RunListComponent } from './run-list/run-list.component';
import { RunFormComponent } from './run-form/run-form.component';
import { RunInfoComponent } from './run-info/run-info.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HeaderLinksComponent,
    UserListComponent,
    UserInfoComponent,
    PersonalBestComponent,
    UserFormComponent,
    RunListComponent,
    RunFormComponent,
    RunInfoComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      {
        path: '',
        redirectTo: 'users',
        pathMatch: 'full'
      },
      {
        path: 'users',
        component: UserListComponent
      },
      {
        path: 'users/create',
        component: UserFormComponent
      },
      {
        path: 'users/:userId',
        component: UserInfoComponent
      },
      {
        path: 'users/:userId/update',
        component: UserFormComponent
      },
      {
        path: 'users/:userId/runs',
        component: RunListComponent
      },
      {
        path: 'users/:userId/runs/add',
        component: RunFormComponent
      },
      {
        path: 'users/:userId/runs/:runId',
        component: RunInfoComponent
      },
      {
        path: 'users/:userId/runs/:runId/update',
        component: RunFormComponent
      }
    ]),
    HttpClientModule,
    FormsModule
  ],
  providers: [
    UserService,
    RunService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
