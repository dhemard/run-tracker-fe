import { Component, OnInit } from '@angular/core';
import { UserService } from '../user-service/user.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from '../user-service/user';

@Component({
  selector: 'rt-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {
  private error: HttpErrorResponse;
  private router: Router;
  private userService: UserService;
  private user: User;
  private userId: number;

  constructor(userService: UserService, router: Router, route: ActivatedRoute) {
    this.userService = userService;
    this.router = router;
    route.params.subscribe(
      params => this.userId = params['userId']
    );
    if (this.userId) {
      userService.getUserById(this.userId).subscribe(
        user => this.user = user,
        error => this.error = error
      );
    } else {
      this.user = new User();
    }
  }

  submitUser() {
    if (this.userId) {
      this.userService.updateUser(this.userId, this.user).subscribe(
        user => this.router.navigate(['users', this.userId]),
        error => this.error = error
      );
    } else {
      this.userService.createUser(this.user).subscribe(
        user => this.router.navigate(['users']),
        error => this.error = error
      );
    }
  }

  deleteUser() {
    this.userService.deleteUser(this.userId).subscribe(
      user => this.router.navigate(['users']),
      error => this.error = error
    )
  }

  ngOnInit() {
  }

}
