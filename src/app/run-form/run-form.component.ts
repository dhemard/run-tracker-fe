import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { RunService } from '../run-service/run.service';
import { Run } from '../run-service/run';

@Component({
  selector: 'rt-run-form',
  templateUrl: './run-form.component.html',
  styleUrls: ['./run-form.component.css']
})
export class RunFormComponent implements OnInit {
  private error: HttpErrorResponse;
  private router: Router;
  private runService: RunService;
  private run: Run;
  private userId: number;
  private runId: number;

  constructor(runService: RunService, router: Router, route: ActivatedRoute) {
    this.runService = runService;
    this.router = router;
    route.params.subscribe(
      params => {
        this.runId = params['runId'];
        this.userId = params['userId'];
      }
    );
    if (this.runId) {
      runService.getRunById(this.userId, this.runId).subscribe(
        run => this.run = run,
        error => this.error = error
      );
    } else {
      this.run = new Run();
    }
  }

  submitRun() {
    if (this.runId) {
      this.runService.updateRun(this.userId, this.runId, this.run).subscribe(
        run => this.router.navigate(['users', this.userId,'runs', this.runId]),
        error => this.error = error
      );
    } else {
      this.runService.createRun(this.userId, this.run).subscribe(
        run => this.router.navigate(['users',this.userId,'runs']),
        error => this.error = error
      );
    }
  }

  deleteRun() {
    this.runService.deleteRun(this.userId, this.runId).subscribe(
      run => this.router.navigate(['users',this.userId,'runs']),
      error => this.error = error
    )
  }

  ngOnInit() {
  }

}
