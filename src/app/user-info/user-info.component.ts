import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

import { UserService } from '../user-service/user.service';
import { User } from '../user-service/user';
import { Best } from '../user-service/best';

@Component({
  selector: 'rt-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class UserInfoComponent implements OnInit {
  private userError: HttpErrorResponse;
  private bestError: HttpErrorResponse;
  private userId: number;
  private user: User;
  private best: Best;

  constructor(userService: UserService, route: ActivatedRoute) {
    route.params.subscribe(
      params => {
        this.userId = params['userId'];
        userService.getUserById(this.userId).subscribe(
          user => this.user = user,
          error => this.userError = error
        );
        userService.getPersonalBest(this.userId).subscribe(
          best => this.best = best,
          error => this.bestError = error
        );
      }
    );
  }

  ngOnInit() {
  }

}
