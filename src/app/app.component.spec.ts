import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { HeaderLinksComponent } from './header-links/header-links.component';
import { HeaderComponent } from './header/header.component';
import { RouterModule } from '@angular/router';
import { UserInfoComponent } from './user-info/user-info.component';
import { RouterTestingModule } from "@angular/router/testing";
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        HeaderComponent,
        HeaderLinksComponent,
        UserInfoComponent
      ],
      imports: [
        RouterTestingModule
      ]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
