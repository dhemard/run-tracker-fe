export class Run {
  id: number;
  userId: number;
  startTime: number;
  elapsed: number;
  distance: number;
  rating: number;
  avgPace: number;
}