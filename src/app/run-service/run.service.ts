import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { environment } from '../../environments/environment';
import { Run } from './run';

@Injectable()
export class RunService {

  private http: HttpClient;
  private rootApiUrl: string = environment.rootApiUrl;
  private userUrl: string = this.rootApiUrl.concat("/users");

  constructor(http: HttpClient) {
    this.http = http;
  }

  getAllRuns(userId: number): Observable<Run[]> {
    return this.http.get<Run[]>(`${this.userUrl}/${userId}/runs`);
  }

  getRunById(userId: number, runId: number): Observable<Run> {
    return this.http.get<Run>(`${this.userUrl}/${userId}/runs/${runId}`);
  }

  createRun(userId: number, run: Run): Observable<Run> {
    return this.http.post<Run>(`${this.userUrl}/${userId}/runs`, run);
  }

  updateRun(userId: number, runId: number, run: Run): Observable<Run> {
    return this.http.put<Run>(`${this.userUrl}/${userId}/runs/${runId}`, run);
  }

  deleteRun(userId: number, runId: number): Observable<Run> {
    return this.http.delete<Run>(`${this.userUrl}/${userId}/runs/${runId}`);
  }

}
