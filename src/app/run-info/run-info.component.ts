import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Run } from '../run-service/run';
import { RunService } from '../run-service/run.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'rt-run-info',
  templateUrl: './run-info.component.html',
  styleUrls: ['./run-info.component.css']
})
export class RunInfoComponent implements OnInit {
  private error: HttpErrorResponse;
  private userId: number;
  private runId: number;
  private run: Run;

  constructor(runService: RunService, route: ActivatedRoute) {
    route.params.subscribe(
      params => {
        this.runId = params['runId'];
        this.userId = params['userId'];
      }
    );
    runService.getRunById(this.userId, this.runId).subscribe(
      run => this.run = run,
      error => this.error = error
    );
  }

  ngOnInit() {
  }

}
