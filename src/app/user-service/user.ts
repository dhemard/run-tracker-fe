export class User {
  id: number;
  username: string;
  firstName: string;
  lastName: string;
  gender: string;
  height: number;
  weight: number;
  dob: number;
}