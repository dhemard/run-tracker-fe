export class Best {
  id: number;
  userId: number;
  distance: number;
  elapsed: number;
  avgPace: number;
}