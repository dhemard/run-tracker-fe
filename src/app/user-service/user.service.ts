import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { environment } from '../../environments/environment';
import { User } from './user';
import { Best } from './best';

@Injectable()
export class UserService {

  private http: HttpClient;
  private rootApiUrl: string = environment.rootApiUrl;
  private url: string = this.rootApiUrl.concat("/users");

  constructor(http: HttpClient) {
    this.http = http;
  }

  getAllUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.url);
  }

  getUserById(userId: number): Observable<User> {
    return this.http.get<User>(`${this.url}/${userId}`);
  }

  createUser(user: User): Observable<User> {
    return this.http.post<User>(this.url, user);
  }

  updateUser(userId: number, user: User): Observable<User> {
    return this.http.put<User>(`${this.url}/${userId}`, user);
  }

  deleteUser(userId: number): Observable<User> {
    return this.http.delete<User>(`${this.url}/${userId}`);
  }

  getPersonalBest(userId: number): Observable<Best> {
    return this.http.get<Best>(`${this.url}/${userId}/bests`);
  }

}
