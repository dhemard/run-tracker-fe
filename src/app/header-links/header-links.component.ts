import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent, NavigationStart } from '@angular/router';

import 'rxjs/add/operator/filter';

@Component({
  selector: 'rt-header-links',
  templateUrl: './header-links.component.html',
  styleUrls: ['./header-links.component.css']
})
export class HeaderLinksComponent implements OnInit {
  private router: Router;
  private currentRoute: string;

  constructor(router: Router) {
    this.router = router;
    this.router.events
      .filter(event => event instanceof NavigationStart)
      .subscribe(
        (event: RouterEvent) => this.setCurrentRoute(event.url)
      );
  }

  ngOnInit() {
  }

  private setCurrentRoute(url: string): void {
    if (url.indexOf('me') > -1) {
      this.currentRoute = 'me';
    } else if (url.indexOf('runs') > -1) {
      this.currentRoute = 'runs';
    }
  }
}
